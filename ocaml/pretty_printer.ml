open ASD

let rec pp_block = function
	| Blockasd(sl) -> print_string "{";
				   print_newline();
				   pp_statement_list sl;
				   print_string "}";
				   print_newline();

and pp_statement_list = function
	| [] -> ()
	| s :: sl -> pp_statement s;
				 print_newline();
				 pp_statement_list sl;

and pp_statement = function
	| SkipStatement -> print_string "SKIP"
	| TraceStatement(e) -> print_string "TRACE ";
						   pp_expression e;

and pp_expression = function
	| AddExpression(e1, e2) -> print_string "(";
							   pp_expression e1;
							   print_string " + ";
							   pp_expression e2;
							   print_string ")";
							   
	| TimesExpression(e1, e2) -> print_string "(";
								 pp_expression e1;
							     print_string " * ";
							     pp_expression e2;
							     print_string ")";
							     
	| MinusExpression(e1, e2) -> print_string "(";
								 pp_expression e1;
							     print_string " - ";
							     pp_expression e2;
							     print_string ")";
							     
	| DivExpression(e1, e2) -> print_string "(";
							   pp_expression e1;
							   print_string " / ";
							   pp_expression e2;
							   print_string ")";
							   
	| IntegerExpression(i) -> print_int i;
	
