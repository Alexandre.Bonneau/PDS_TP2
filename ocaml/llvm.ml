(* TODO : extend when you extend the language *)

(* This file contains a simple LLVM IR representation *)
(* and methods to generate its string representation  *)

open List

type llvm_type =
  | LLVM_Type_Int


(* Warning: because of type inference, we can not
 * have the same field name in two record type
 * (actually this is possible but cumbersome)
 *)

(* fields in each instruction *)
type llvm_binop = {
  lvalue_name: string;
  lvalue_type: llvm_type;
  op: string;
  left: string;
  right: string;
}
and llvm_return = {
  ret_type: llvm_type;
  ret_value: string; (* we only return identifier, or integers as int *)
}

and llvm_stmt = {
  lvalue_name: string;
  lvalue_type: llvm_type;
  block: string;
}

and llvm_block = {
  lvalue_name: string;
  lvalue_type: llvm_type;
  head: string;
  tail: string;
   h : llvm_instr list;
   h2 : llvm_instr list;
}

(* instructions sum type *)
and llvm_instr =
  | Stmt of llvm_stmt
  | Binop of llvm_binop
  | Return of llvm_return
  | Blocks  of llvm_block

(* Note: instructions in list are taken in reverse order in
 * string_of_ir in order to permit easy list construction !!
 *)
and llvm_ir = {
  header: llvm_instr list; (* to be placed before all code (global definitions) *)
  code: llvm_instr list;
}

(* handy *)
let empty_ir = {
  header = [];
  code = [];
}

(* actual IR generation *)
let rec string_of_llvm_type = function
  | LLVM_Type_Int -> "i32"
and string_of_ir ir =
  (string_of_instr_list ir.header) ^ "\n\n" ^ (string_of_instr_list ir.code)

and string_of_instr_list l =
  String.concat "" (rev_map string_of_instr l)

and string_of_instr = function
  | Binop v -> v.lvalue_name ^ " = " ^ v.op ^ " " ^ (string_of_llvm_type v.lvalue_type) ^ " " ^ v.left ^ ", " ^ v.right ^ "\n"
  | Stmt v ->  "tail call void @printint(" ^(string_of_llvm_type v.lvalue_type)^" " ^ v.lvalue_name ^  ")\n "
  | Return v ->  "ret i32 0 \n" (* ^ (string_of_llvm_type v.ret_type) ^ " " ^ v.ret_value ^ "\n"*)
  | Blocks v-> ""

      (*"ret " ^ (string_of_llvm_type v.ret_type) ^ " " ^ v.ret_value ^ "\n"*)
(* "tail call void @printint(" ^(string_of_llvm_type v.ret_type)^" " ^ v.ret_value ^  ")\n ret i32 0" *)
