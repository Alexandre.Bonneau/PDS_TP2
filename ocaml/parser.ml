open ASD
open Token

(* p? *)
let opt p = parser
  | [< x = p >] -> Some x
  | [<>] -> None

(* p* *)
let rec many p = parser
  | [< x = p; l = many p >] -> x :: l
  | [<>] -> []

(* p+ *)
let some p = parser
  | [< x = p; l = many p >] -> x :: l

(* p (sep p)* *)
let rec list1 p sep = parser
  | [< x = p; l = list1_aux p sep >] -> x :: l
and list1_aux p sep = parser
  | [< _ = sep; l = list1 p sep >] -> l
  | [<>] -> []

(* (p (sep p)* )? *)
let list0 p sep = parser
  | [< l = list1 p sep >] -> l
  | [<>] -> []


(* --------------- *)
(* Parses programs *)
(* --------------- *)
let rec program = parser
	| [< pul = program_unit_list >] -> pul

and program_unit_list = parser
	| [< (*empty*) >] -> []
	| [< pu = program_unit ; pul = program_unit_list >] -> pu :: pul  

and program_unit = parser
	| [< 'PROTO_KW ; p = proto >] -> Proto(p)
	| [< 'FUNC_KW   ; p = proto?? "proto" ; b = block?? "block" >] -> Function(p, b)

and proto = parser
	| [< t = typ ; id = ident ; 'LP ; vdf = var_decl_func ; 'RP >] -> (t, id, vdf)

and block = parser
	| [< 'LB ; sl = statement_list ; 'RB>] -> Blockasd(sl:ASD.statement list)
(*	| [< 'LB ; sl = statement_list?? "statementlist" ; 'RB?? "pq"  >] -> Blockasd(sl:ASD.statement list) *)
	(*NEXT : | [< 'LB ; d= declaration ; sl = statement_list ; 'RB >] -> Block(d, sl)*)


and statement_list = parser
	| [< s = statement ; sl = statement_list >] -> s :: sl
	| [< (*empty*) >] -> []

(* ------------ *)
(* Parses types *)
(* ------------ *)
and typ = parser
	| [< 'VOID_KW >] -> Type_Void
	| [< 'INT_KW >] -> Type_Int

(* ------------- *)
(* Parses idents *)
(* ------------- *)
and ident = parser
	| [< 'IDENT id >] -> id
	
(* ------------------------------------------- *)
(* Parses variable announcements in prototypes *)
(* like fact(n)                                *)
(* ------------------------------------------- *)
and var_decl_func = parser
	| [< idl = ident_list >] -> VariableAnnouncement(idl) 


(* ------------------------------------- *)
(* Parses variable declaration in blocks *)
(* ------------------------------------- *)
and declaration = parser
	| [< (*empty*) >] -> []
	| [< di = dec_item ; d = declaration >] -> di :: d

and dec_item = parser
	| [< t = typ ; idl = ident_list_non_empty >] -> VariableDeclaration(t, idl)


(* ------------------------------------- *)
(* Parses ident list empty and non empty *)
(* ------------------------------------- *)
and ident_list = parser
	| [< (*empty*) >] -> []
	| [< id = ident ; idl = ident_list >] -> id ::n idl

and ident_list_non_empty = parser
	| [< id = ident >] -> [id]
	| [< id = ident ; idl = ident_list_non_empty >] -> id :: idl

(* ----------------- *)
(* Parses statements *)
(* ----------------- *)
and statement = parser
	| [< 'SKIP_KW >] -> SkipStatement
	| [< 'TRACE_KW   ; e = expression ?? "statement"  >] ->TraceStatement(e)
	
(* ------------------ *)
(* Parses expressions *)
(* ------------------ *)
and expression = parser
  | [< e1 = factor; e = expression_aux e1 >] -> e

and expression_aux e1 = parser
  | [< 'MUL;   e2 = factor; e = expression_aux (TimesExpression (e1, e2)) >] -> e
  | [< 'DIV;   e2 = factor; e = expression_aux (DivExpression (e1, e2)) >] -> e
  | [< 'PLUS;  e2 = factor; e = expression_aux (AddExpression (e1, e2)) >] -> e
  | [< 'MINUS; e2 = factor; e = expression_aux (MinusExpression (e1, e2)) >] -> e
  | [<>] -> e1

and factor = parser
  | [< e1 = primary; e = factor_aux e1 >] -> e
  | [< 'LP;e1 = factor; e = expression_aux e1 ; 'RP>] -> e

and factor_aux e1 = parser
  | [<>] -> e1

and primary = parser
  | [< 'INTEGER x >] -> IntegerExpression x

and comma = parser
  | [< 'COMMA >] -> ()
