export SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

#tests every positive test on expression
for file in `ls $SCRIPTPATH/tests/expression/ | grep pos_test | grep expr`;
do
	echo ---- TESTING $file ----
	cat $SCRIPTPATH/tests/expression/$file
	echo
	
	#converts the expression in a block
	$SCRIPTPATH/tests/expr_to_stmt.sh $SCRIPTPATH/tests/expression/$file temp
	$SCRIPTPATH/tests/stmt_to_block.sh temp temp

	#generates the binary file
	$SCRIPTPATH/main.native < temp > temp.ll
	rm temp
	clang temp.ll -o ./test
	rm temp.ll

	#saves the execution result
	filename="${file%.*}"
	./test > $SCRIPTPATH/tests/expression/$filename.result
	rm ./test

	nbrDif=`diff -U 0 $SCRIPTPATH/tests/expression/$filename.expected $SCRIPTPATH/tests/expression/$filename.result | grep -v ^@ | wc -l`

	echo
	if [ "$nbrDif" == "0" ];
	then
		echo TEST SUCCESS
	else
		echo TEST FAILED
	fi
	echo
done



#tests every negative test on expression
for file in `ls $SCRIPTPATH/tests/expression/ | grep neg_test | grep expr`;
do
	echo ---- TESTING $file ----
	cat $SCRIPTPATH/tests/expression/$file
	echo
	
	#converts the expression in a block
	$SCRIPTPATH/tests/expr_to_stmt.sh $SCRIPTPATH/tests/expression/$file temp
	$SCRIPTPATH/tests/stmt_to_block.sh temp temp

	#try to generates the binary file
	$SCRIPTPATH/main.native < temp > temp.ll
	output=$(cat temp.ll)
	rm temp.ll

	echo
	if [ "$output" == "" ];
	then
		echo TEST SUCCESS
	else
		echo TEST FAILED
	fi
	echo
done
