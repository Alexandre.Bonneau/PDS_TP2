(* TODO : extend when you extend the language *)

type ident = string

type expression =
	| AddExpression of expression * expression
	| TimesExpression of expression * expression
	| MinusExpression of expression * expression
	| DivExpression of expression * expression
	| IntegerExpression of int
	
type typ =
	| Type_Int
	| Type_Void



type program = program_unit list

and
	program_unit =
		| Function of (proto * blockasd)
		| Proto of proto

and	
		proto= (typ * ident * var_decl_func)  (* typ * function's_name  * args*)
		(* proto : function declaration *)

and
	blockasd = Blockasd of (statement list) (* TODO : add declaration *)
	(* block = Block of (declaration list * statement list) *)

and
	statement =
		(*|AssignStatement of ( ident * expression )*)
		|SkipStatement (* pour compiler facilement au début, INUTILE ?*)
		|TraceStatement of (expression)(* affiche une expression *)

and
	declaration = 
		|VariableDeclaration of (typ * ident list)
		
and
	var_decl_func = (* Announcement of the variable in a function declaration *)
		| VariableAnnouncement of (ident list)
