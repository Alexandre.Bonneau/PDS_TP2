open Lexer
open Parser
open ASD
open Codegen
open Pretty_printer

let lexbuf = Lexing.from_channel stdin in
try
  let token_stream = (Stream.of_list (Lexer.tokenize lexbuf)) in
  (*let ast = Parser.expression token_stream*)
  let astBlock = Parser.block token_stream in
  (*
  print_newline();
  print_string "----- PRETTY PRINTER -----";
  print_newline();
  Pretty_printer.pp_block astBlock;
  print_newline();
  *)
  let document = ir_of_ast astBlock in
  print_endline document;
with
  Lexer.Unexpected_character e ->
  begin
    Printf.printf "Unexpected character: `%c' at position '%d' on line '%d'\n"
      e.character e.pos e.line;
    exit 1
  end



